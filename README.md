Blast Media Inc. is a Vancouver owned and operated printing and promotions company dedicated to helping businesses elevate their brand with high quality, professional print solutions. Your trusted partner for large format printing, trade show displays, window graphics, security window film and more!

Address: 81 West Pender St, Unit D, Vancouver, BC V6B 1R3, Canada

Phone: 877-967-7468

Website: http://www.blastmediainc.com